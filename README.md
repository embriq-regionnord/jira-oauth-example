# Introduksjon autentisering

## Om instansene

Plussplan er til alle praktiske formål helt vanlige instanser av Jira Server og Conflucene Server (altså ikke Cloud). Disse instansene har noe skreddersøm i form at logo, fargepalett og enkelte plugin-utvidelser.

## Jira OAuth

Tips:

* Når dere benytter dokumentasjonen til Atlassian, vær klar over at Jira Server og Jira Cloud ikke fungerer likt (Plussplan er Jira Server). 
* For omtrent ett år siden begynte Altassian overgangen til en ny dokumentasjonsform der artiklene blant annet er daterte. U-daterte artikler kan derfor i mange tilfeller være relativt gamle.

Lenker:

* [Her finner dere flere eksempler på OAuth for ulike språk](https://bitbucket.org/atlassian_tutorial/atlassian-oauth-examples/src/default/) (kodeeksempelet vedlagt er basert på disse)
* [Generelt om autentisering](https://developer.atlassian.com/jiradev/jira-apis/jira-rest-apis/jira-rest-api-tutorials/jira-rest-api-example-cookie-based-authentication)
* [Pålogging med Oauth](https://developer.atlassian.com/server/jira/platform/oauth/)
* [Pålogging med Basic Auth](https://developer.atlassian.com/server/jira/platform/basic-authentication/)


## Confluence OAuth

Ulike nettressurser tyder på at OAuth-autentisering for Confluence og Jira skal fungere stort sett likt, med enkelte unntak (slik som auth-url). Vi har ingen direkte erfaring med autentisering mot Confluence gjennom OAuth.

* [Community-svar på manglende dokumentasjon av auth-URL (se John Tolle sine svar)](https://community.atlassian.com/t5/Questions/Confluence-Oauth-Authentication/qaq-p/331326)
* [REST-API i Confluence](https://developer.atlassian.com/server/confluence/confluence-server-rest-api/)

## Plugins for Confluence Jira som implementerer SSO:

Det eksisterer som nevnt også ferdiglagede plugins for Jira/Confluence som gir mulighet for pålogging med bla. Open ID. De passer kanskje ikke inn i måten Plussplan benytter delte brukerkonti (av lisensårsaker?) for Jira/Confluence, men er verdt å kjenne til:

* [Confluence - Open ID](https://community.atlassian.com/t5/Questions/Confluence-Oauth-Authentication/qaq-p/331326)
* [Jira - Open ID](https://marketplace.atlassian.com/plugins/com.pawelniewiadomski.jira.jira-openid-authentication-plugin/server/overview)
* [Øvrige alternativer](https://marketplace.atlassian.com/search?query=SSO)

