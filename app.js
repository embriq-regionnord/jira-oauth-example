let settings = require("./settings.js");
let restHelper = require("./restHelper.js");

console.log("# Using configuration: ", settings);


/**
 * Koden under forsøker å logge inn på admin-panelet til Jira med administratorbrukeren definert i settings-panelet.
 * 
 * restHelpers.js   -    inneholder enkle hjelpemetoder for client-håndtering
 * settings.js      -    inneholder informasjon knyttet til serverURL, bruker ol.
 * 
 * Se README.md for generell informasjon og tips.
 */
let someSecureUrl = "http://localhost:8081/jira/secure/admin/ViewApplicationProperties.jspa";

restHelper.callSomeInternalJiraUrl(someSecureUrl);