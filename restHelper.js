// Code based on: https://developer.atlassian.com/jiradev/jira-apis/jira-rest-apis/jira-rest-api-tutorials/jira-rest-api-example-cookie-based-authentication

var exports = module.exports = {};
let settings = require("./settings.js");
let Client = require('node-rest-client').Client;
    
client = new Client();

let getLoginArgs = () => {
    return {
        data: {
            "username": settings.jiraUsername,
            "password": settings.jiraUserPassword
        },
        headers: {
            "Content-Type": "application/json"
        } 
    };
}

let getAuthenticatedArgs = (session) => {
    return {
        data: {
        },
        headers: {
            cookie: session.name + '=' + session.value,
            "Content-Type": "application/json"
        },
    };
}

let loginAndExecute = (fun) => {
    client.post(settings.jiraOAuthEndpointUrl, getLoginArgs(), (data, response) => {
        if (response.statusCode !== 200) throw "Login failed! Wrong username or password?";

        let authenticatedArgs = getAuthenticatedArgs(data.session);
    
        fun(authenticatedArgs);
    }).on("error", (error) => {
        console.log("LOGIN ERROR: ", error);
    });
}

exports.callSomeInternalJiraUrl = (url) => {
    loginAndExecute((authenticatedArgs) => {

        client.get(url, authenticatedArgs, (result, response) => {
            console.log(`# Called URL: ${url}`);
            console.log(`# And got status code: ${response.statusCode}`);
            console.log(`# When sending header: ${response.req._header}`);
        }).on("error", (error) => {
            console.log("REST CALL ERROR: ", error);
        });

    });
}