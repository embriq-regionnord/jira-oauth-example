module.exports = (() => {

    let baseUrl = "http://localhost:8081/jira";

    return {
        jiraUsername: "adminUserName",
        jiraUserPassword: "adminPassword",
        baseUrl,
        jiraOAuthEndpointUrl: baseUrl + "/rest/auth/1/session"
    }; 
})()



